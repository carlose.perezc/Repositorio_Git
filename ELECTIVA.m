%fuction [values] = DadosTest()
close all;
clear all;
clc;

oldobj = instrfind;
if not(isempty(oldobj))
	fclose(oldobj);
	delete(oldobj);
end

if ~exist('s','var')
	s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if ~isvalid(s)
	s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if strcmp(get(s,'status'),'closed')
	fopen(s);
end

SENSITIVITY_ACCEL = 2.0/32768.0;
SENSITIVITY_GYRO = 250.0/32768.0;

offset_accelx =-2594.00;
offset_accely =-256.00;
offset_accelz = 16088.00;
offset_gyrox = 143.50;
offset_gyroy = -160.50;
offset_gyroz =0.00;

disp('En sus marcas. posicione el sensor en la posicion inicial')
pause();

disp('comienza')
fprintf(s,'H');
i = 1;

while(1)
	str{i}=fscanf(s);
	if(str{i}(1) == 'A')
		disp('termina')
		break;
    end
    i = i+1;
end

fclose(s);
n = length(str)-1;
c = 1;

for i=1:n
	temp = cellfun(@str2num,strsplit(str{i},','));
	if numel(temp)==8
		values(i,:) =temp;
	end
end

save HOLA values

Nsamples = length(values);
dt = 0.01;
t = 0:dt:Nsamples*dt-dt;

figure;
plot(t, values(:,3)*SENSITIVITY_ACCEL, 'b')
hold on
plot(t, values(:,4)*SENSITIVITY_ACCEL, 'r');
plot(t, values(:,5)*SENSITIVITY_ACCEL, 'g');
title('Acelerometros da MPU9250 SEM calibracao')
ylabel('aceleracao (g)')
legend('ax', 'ay', 'az', 'Location', 'northeast', 'Orientation', 'horizantal')

figure;
plot(t, (values(:,3)-offset_accelx)*SENSITIVITY_ACCEL, 'b')
hold on
plot(t, (values(:,4)-offset_accely)*SENSITIVITY_ACCEL, 'r');
plot(t, (values(:,5)-(offset_accelz-(32768/2)))*SENSITIVITY_ACCEL, 'g');
title('Acelerometros da MPU9250 calibrados')
ylabel('aceleracao (g)')
xlabel('Tempo (segundos)')
legend('ax', 'ay', 'az', 'Location', 'northeast', 'Orientation', 'horizantal')

figure;
plot(t, values(:,6)*SENSITIVITY_GYRO, 'b')
hold on
plot(t, values(:,7)*SENSITIVITY_GYRO, 'r');
plot(t, values(:,8)*SENSITIVITY_GYRO, 'g');
title('Giroscopios da MPU9250 sem calibracao')
ylabel('Velocidade angular (�/s)')
xlabel('Tempo (segundos)')
legend('gx', 'gy', 'gz', 'Location', 'southeast', 'orientation', 'horizontal')

figure;
plot(t, (values(:,6)-offset_gyrox)*SENSITIVITY_GYRO, 'b')
hold on
plot(t, (values(:,7)-offset_gyroy)*SENSITIVITY_GYRO, 'r');
plot(t, (values(:,8)-offset_gyroz)*SENSITIVITY_GYRO, 'g');
title('Acelerometros da MPU9250 calibrados')
ylabel('Velocidade angular (�/s)')
xlabel('Tempo (segundos)')
legend('gx', 'gy', 'gz','Location', 'northeast', 'Orientation', 'horizantal')
